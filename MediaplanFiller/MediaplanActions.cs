﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace MediaplanFiller
{

    class MediaplanActions
    {

        private static Messages message = Messages.GetInstance();

        public static Mediaplan LoadMediaplan(string excelFilename, bool print=false){
            var excelApp = new Excel.Application();
            try
            {
                Excel.Workbook Workbook = excelApp.Workbooks.Open(excelFilename);
                Excel.Sheets Sheets = Workbook.Worksheets;
                int WCount = Sheets.Count;
                Mediaplan Mplan = new Mediaplan();
                for (int i = 1; (i-1) < WCount; i++)
                {
                    Mplan.Blocks.AddRange(ParseDay(Sheets[i], i));
                }
                if (print)
                {
                    Print(Mplan);               
                }
                Marshal.FinalReleaseComObject(Sheets);
                Marshal.FinalReleaseComObject(Workbook);
                Workbook = null;
                Sheets = null;
                return Mplan;
            }
            catch (Exception)
            {
                message.AddMessage($"Не удалось загрузить медиаплан. " );
                return null;
            }
            finally
            {
                excelApp.Quit();
                Marshal.FinalReleaseComObject(excelApp);
                excelApp = null;
                GC.Collect();
            }
        }

        private static List<Mediaplan.Block> ParseDay(Excel.Worksheet Sheet, int DayOfWeek)
        {
            int order = 0;
            string oldTime = "";
            string Date = Sheet.Cells[1, 2].Text;
            List<Mediaplan.Block> Blocks = new List<Mediaplan.Block>();
            Console.WriteLine(Date);

            for (int n = 3; n < 65534; n++)
            {
                var cell1 = Sheet.Cells[n, 1].Text;
                var cell2 = Sheet.Cells[n, 2].Text;
                var cell3 = Sheet.Cells[n, 3].Text;
                var cell4 = Sheet.Cells[n, 4].Text;
                var cell5 = Sheet.Cells[n, 5].Text;

                if (cell1 == "" && cell2 == "" && cell5 == "")
                {
                    break;
                }

                if (!(Sheet.Cells[n, 4].Value is null))
                {
                    string[] time = cell1.Split(':');
                    string curTime = time[0];
                    if ( curTime != oldTime)
                        order = 0;
                    oldTime = curTime;
                    order++;
                    Console.WriteLine(Sheet.Cells[n, 1].Text + "    " + Sheet.Cells[n, 2].Text + "    " + Sheet.Cells[n, 3].Text + "    " + Sheet.Cells[n, 4].Text + "    " + Sheet.Cells[n, 5].Text);
                    Dictionary<string, int> curDuration = GetTime(cell4);
                    
                    // if duration of adv more then hour iteration will skip (костыль п-ц, а что делать)
                    // adeed to leave 7th hour mistakes at the end of a list
                    if (curDuration["Hour"] > 0 || curDuration["Minute"] > 30)
                        continue;
                    Mediaplan.Block newBlock = new Mediaplan.Block(Date, DayOfWeek, time[0], time[1], order, (curDuration["Minute"] * 60 + curDuration["Second"]));
                    Blocks.Add(newBlock);
                } 
            }
            Marshal.FinalReleaseComObject(Sheet);
            Sheet = null;
            return Blocks;
        }

        private static void Print(Mediaplan Mplan) {
            var Blocks = from t in Mplan.Blocks orderby t.Date, t.Hour, t.Order select t;
            int i=1;
            foreach (var block in Blocks)
            {
                message.AddMessage($"Дата:{block.Date}\t№{block.Order}\tЧас: {block.Hour}:{block.Minutes}\tДлит.: {block.Duration} \n");
                i++;
                if (i % 100 == 0)
                    Console.ReadLine();
            }
            Console.ReadLine();
        }

        private static Dictionary<string, int> GetTime(string timeString)
        {
            Dictionary<string, int> Time = new Dictionary<string, int>();
            try
            {
                string[] keys = { "Hour", "Minute", "Second" };
                string[] values = timeString.Split(new char[] { ':' });
                for (int i = 0; i < values.Length; i++)
                {
                    Time.Add(keys[i], Convert.ToInt16(values[i]));
                }
            }
            catch (Exception)
            {
                message.AddError($"Не удалось извлечь время из строки {timeString}. Проверьте правильность заполнения медиаплана.");
                throw;
            }
            return Time;
        }
    }
}
