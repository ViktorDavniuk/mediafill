﻿using Fclp;

namespace MediaplanFiller
{
    class ApplicationArguments
    {
        public string Mediaplan { get; set; }
        public string BGR { get; set; }
        public string OutFile { get; set; }
    }

    class CLArgumentsParser
    {
        public static ApplicationArguments Parse(string[] args)
        {
            ApplicationArguments arguments = new ApplicationArguments();
            var parser = new FluentCommandLineParser<ApplicationArguments>();
            var p = new FluentCommandLineParser();
            parser.Setup(arg=>arg.Mediaplan).As('m', "mediaplan");
            parser.Setup(arg=>arg.OutFile).As('o', "outfile");
            parser.Setup(arg => arg.BGR).As('b', "bgr");
            var result = parser.Parse(args);
            if (result.HasErrors == false ) {
                return parser.Object;
            } else
            {
                return new ApplicationArguments();
            }
        }
    }
}
