﻿using System;
using System.Collections.Generic;

namespace MediaplanFiller
{
    class Mediaplan
    {

        public List<Block> Blocks = new List<Block>();

        public class Block
        { 
            public DateTime Date { get; set; }
            public int DayOfWeek { get; set; }
            public string Hour { get; set; }
            public string Minutes { get; set; }
            public int Order { get; set; }
            public int Duration { get; set; }

                public Block(string date, int dayOfWeek, string hour, string minutes, int order, int duration)
                {
                    this.Date = DateTime.ParseExact(date, "dd.mm.yy", null);
                    this.DayOfWeek = dayOfWeek;
                    this.Hour = hour;
                    this.Minutes = minutes;
                    this.Order = order;
                    this.Duration = duration;
                }
        }
    }    
}
