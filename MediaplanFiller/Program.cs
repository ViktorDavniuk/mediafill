﻿namespace MediaplanFiller
{
    class Program
    {
        static void Main(string[] args)
        {
            ApplicationArguments Arguments = CLArgumentsParser.Parse(args);
            Mediaplan MP = MediaplanActions.LoadMediaplan(Arguments.Mediaplan);
            string file = BGRActions.ReadFile(Arguments.BGR);
            string processed = BGRActions.ProcessFile(file, MP);
            BGRActions.SaveFile(Arguments.OutFile, processed);
        }
    }
}
