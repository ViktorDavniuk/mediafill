﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;

namespace MediaplanFiller
{
    class BGRActions
    {
        public static string ReadFile(string filename) {
            string line;
            try
            {
                using (StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("windows-1251")))
                {
                    line = sr.ReadToEnd(); 
                }
                return line;
            }
            catch (Exception e)
            {
                Console.WriteLine("Невозможно прочесть файл!");
                Console.WriteLine(e.Message);
                return null;
            }
        }
    
        public static string ProcessFile(string inputText, Mediaplan plan)
        {
            string outputText = "";
            List<BGR.Prog> ProgsList = new List<BGR.Prog>();
            List<string> GridsList = new List<string>();
            Dictionary<string, string> Progs = new Dictionary<string, string>();
            outputText += Regex.Match(inputText, @"(?<mainText>\[Main\][^\[]*)").Groups["mainText"].ToString();
            MatchCollection ProgsMatches = Regex.Matches(inputText, @"\[PROG(?<progOrder>\d+)\](?<progText>[^\[]*)");
            foreach (Match item in ProgsMatches)
            {
                ProgsList.Add(ParseProg(item.ToString(), Convert.ToInt16(item.Groups["progOrder"].ToString())));
                outputText += item.ToString();
            }
            MatchCollection GridsMatches = Regex.Matches(inputText, @"\[\d+\](?<gridText>[^\[]*)");
            foreach (Match item in GridsMatches)
            {
                outputText += ParseGrid(item.ToString(), ProgsList, plan);
            }
            return outputText;
        }

        public static BGR.Prog ParseProg(string progText, int progOrder)
        {
            BGR.Prog newProg = new BGR.Prog();
            newProg.order = progOrder;
            newProg.id = Regex.Match(progText, @"ProgDataPROG_ID=(?<id>\d+)").Groups["id"].ToString();
            if (newProg.id != "")
            {
                Match artTime = Regex.Match(progText, @"ProgDataPROG_ART=(?<hour>\d+)_(?<block>\d+)");
                newProg.hour = artTime.Groups["hour"].ToString();
                newProg.block = Convert.ToInt16(artTime.Groups["block"].ToString());
            }
            return newProg;
        }

        public static string ParseGrid(string gridText, List<BGR.Prog> progsList, Mediaplan plan)
        {
            string GridReplacement1 = "";
            string GridReplacement2 = "";
            string ProgId = Regex.Match(gridText, @"Bl_GridDataPROG_ID=(?<progId>\d+)").Groups["progId"].ToString();
            BGR.Prog CurrentProg = progsList.First<BGR.Prog>(t => t.id == ProgId);
            var Blocks = from t in plan.Blocks
                         where t.Hour == CurrentProg.hour &&
                         t.Order == CurrentProg.block
                         select t;
            for (int i = 1; i < 8; i++)
            {
                Mediaplan.Block CurrentBlock = Blocks.FirstOrDefault<Mediaplan.Block>( t => t.DayOfWeek == i);
                if (CurrentBlock == null) {
                    GridReplacement1 += $"Bl_GridDataW{i}_LENGTH=60\r\n";
                    GridReplacement2 += $"Bl_GridDataW{i}=False\r\n";
                }
                else
                {
                    GridReplacement1 += $"Bl_GridDataW{i}_LENGTH={CurrentBlock.Duration}\r\n";
                    GridReplacement2 += $"Bl_GridDataW{i}=True\r\n";
                }
            }
            string result = Regex.Replace(gridText, @"(Bl_GridDataW1_LENGTH=[\s\S]*?)Bl_GridDataBL_GRIDS_ID", $"{GridReplacement1}{GridReplacement2}Bl_GridDataBL_GRIDS_ID");
            return result;
        }

        public static void SaveFile(string filename, string source)
        {
            using (StreamWriter sw = new StreamWriter(filename, false, Encoding.GetEncoding("windows-1251")))
            {
                sw.Write(source);
            }
        }
    }
}
