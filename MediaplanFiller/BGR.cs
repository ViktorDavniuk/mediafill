﻿using System;

namespace MediaplanFiller
{
    class BGR {
        public class Prog
        {
            public int order { get; set; }
            public string id { get; set; }
            public string hour { get; set; }
            public int block { get; set; }
            public void ProgArt (string progart) {
                if (progart != "")
                {
                    string[] s = progart.Split('_');
                    hour = s[0];
                    block = Convert.ToInt16(s[1]);
                }
            }
        }
        public class Grid
        {
            public int order { get; set; }
            public string[] text { get; set; }
        }
    }
}
